package org.deblauwe.jira.plugin.databasevalues;

/**
 * Created by IntelliJ IDEA.
 * User: wdb
 * Date: Sep 12, 2010
 * Time: 8:16:12 AM
 * To change this template use File | Settings | File Templates.
 */
public enum DatabaseRowCachePurpose
{
	EDIT, SEARCH
}
